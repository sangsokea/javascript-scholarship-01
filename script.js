"use strict";
const data = [
  {
    id: 1,
    title: "Fort Lauderdale, FL, United States",
    image:
      "https://images.pexels.com/photos/5029795/pexels-photo-5029795.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    description:
      "Blue and White Concrete Building Near Body of Water During Daytime ",
  },
  {
    id: 2,
    address: "Bali, Indonesia",
    image:
      "https://images.pexels.com/photos/5933064/pexels-photo-5933064.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    description: "Bird's Eye View of an Island",
  },
  {
    id: 3,
    address: "Madrid, Community of Madrid, Spain",
    image:
      "https://images.pexels.com/photos/4353813/pexels-photo-4353813.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    description:
      "Anonymous tourists showing US passports on street on sunny day",
  },
  {
    id: 4,
    address: "Turkey",
    image:
      "https://images.pexels.com/photos/5933064/pexels-photo-5933064.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    description: "People Sitting On Pillows Using Shisha",
  },
  {
    id: 5,
    address: "Turkey",
    image:
      "https://images.pexels.com/photos/2739305/pexels-photo-2739305.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    description: "Man and Woman Walking on Road ",
  },
  {
    id: 6,
    address: "Turkey",
    image:
      "https://images.pexels.com/photos/2536587/pexels-photo-2536587.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
    description: "Photo of Couple Walking on Road Near Bare Trees",
  },
  
];
// select element

